# Extensive Enterprise Solutions
## LIS 4369

## Sydney Sawyer

### Project 1 Requirements:

*Three Parts:*

1. Backward-engineer Data Analysis 1 (Python)
2. Skillsets (7-9)
3. Bitbucket repo links:

    a) this project and

    b) Skillsets Folder

#### README.md file should include the following items:

* Screenshot of p1_data_analysis_1 application running
* Link to P1 .ipynb file: [p1_data_analysis.ipynb](p1_data_analysis/p1_data_analysis.ipynb "P1 Jupyter Notebook")
* Screenshots of Skillsets (7-9) 


#### Project Screenshots:

|*Data Analysis Screenshot 1*: |*Data Analysis Screenshot 2*: 	|
|:-:	|:-:	|
| ![P1 Screenshot 1](img/p1_screenshot1.png) 	|![P1 Screenshot 2](img/p1_screenshot2.png) 	|

**Data Analysis 1 Graph**:
![Data Analysis 1 Graph](img/p1_screenshot3.png)
####

**P1 Jupyter Notebook**:

|*Part 1*: |*Part 2*: 	|
|:-:	|:-:	|
| ![P1 in Jupyter Notebook 1](img/p1_jupyter_notebook1.png) 	|![P1 in Jupyter Notebook 2](img/p1_jupyter_notebook2.png) 	|

**Skillset Screenshots**:

|Skillset 7 |Skillset 8 	|
|:-:	|:-:	|
|![Skillset 7](img/ss7_screenshot.png) 	|![Skillset 8](img/tuples8_screenshot.png) 	|


|Skillset 9 p.1 |Skillset 9 p.2 	|
|:-:	|:-:	|
|![Skillset 9 p.1](img/ss9_screenshot1.png) 	|![Skillset 9 p.2](img/ss9_screenshot2.png) 	|


#### Links:

*This Project - lis4369*:
[P1 for lis4369](https://bitbucket.org/sls16d/lis4369/src/master/p1 "P1 Link")

*Skillsets Folder*:
[Skillsets Folder](https://bitbucket.org/sls16d/lis4369/src/master/skill_sets/ "Skillsets Link")

*P1 Jupyter Notebook .ipybn*:
[p1_data_analysis.ipynb](p1_data_analysis/p1_data_analysis.ipynb "P1 Jupyter Notebook")
