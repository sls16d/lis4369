# Notes: If the following file doesn't work, research why! :)
# Hint: You may need to do some intallations!
# BE SURE teh necessary packages are intsalled! pip ="Pip Installs Packages"
# pip freeze 

import pandas as pd # Pandas = "Python Data Analysis Library"
import datetime
import pandas_datareader as pdr # remote access for pandas 
import matplotlib.pyplot as plt 
from matplotlib import style 

start = datetime.datetime(2010, 1, 1)
end = datetime.datetime(2018, 10, 15)

# Read data in to Pandas DataFrame
# Note: OXM is stock market symbol for Exxon Mobil Corp. 
df = pdr.DataReader("XOM", "yahoo", start, end)

print("\nPrint number of records: ")


# Why is it important to run the following print statement... 
print(df.columns)

print("\nPrint data frame:")
print(df) # Note: for efficiency, only prints 60 -- not *all* records

print("\nPrint first five lines:")
# Note: "Date" is lower than other columns as it is treated as an index 
# statement goes here ... 

print("\nPrint first 2 lines:")
#statement goes here ... 

print("\nPrint last 2 lines:")
#statement goes here... 

# Research what these styles do!
# style.use('fivethirtyeight')
# compare with...
style.use('ggplot')

df['High'].plot()
df['Adj Close'].plot()
plt.legend()
plt.show()
