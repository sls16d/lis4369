"""Defines two functions 
1. get_requirements()
2. data_analysis_1()
"""
import datetime
# import pandas as pd # Pandas = "Python Data Analysis Library"
import pandas_datareader as pdr # remote data access for pandas 
import matplotlib.pyplot as plt 
from matplotlib import style


def get_requirements():
    print("Data Analysis 1")
    print("Developer: Sydney Sawyer")

    print("\nProgram Requirements:\n"
        + "1. Run demo.py.\n"
        + "2. If errors, more than likely missing installations.\n"
        + "3. Test Python Package installer: pip freeze\n"
        + "4. Research how to do the following installations:\n"
            + "\ta. pandas (only if missing)\n"
            + "\tb. pandas-datareader (only if missing)\n"
            + "\tc. matplotlib (only if missing)\n"
        + "5. Create at least three functions that are called by the program:\n"
            + "\ta. main(): calls at least two other functions.\n"
            + "\tb. get_requirements(): displays the program requirements.\n"
            + "\tc. data_analysis_1(): displays the follwing data.\n") 


def data_analysis_1(): 
    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime.now()

    # Read data into Pandas DataFrame, implicitly created by DataReader 
    # NOTE: XOM is stock market symbol for Exxon Mobil Corporation 

    df = pdr.DataReader("XOM", "yahoo", start, end)

    print("\nPrint number of records: ")
    print(len(df))

    print("\nPrint columns: ")
    print(df.columns)

    print("\nPrint data frame: ")
    print(df)

    print("\nPrint first five lines:")
    print(df.head()) # head() prints top 5 rows

    print("\nPrint last five lines:")
    print(df.tail())

    print("\nPrint first two lines:")
    print(df.head(2))

    print("\nPrint last two lines:")
    print(df.tail(2))

    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()





