# Extensive Enterprise Solutions
## LIS 4369

## Sydney Sawyer

### Project 2 Requirements:

*Four Parts:*

1. Backward-engineer lis4369_p2_requirements.txt
2. Provide two plots
3. Include screenshots of all work
4. Bitbucket repo links:

    a) this project

#### README.md file should include the following items:

* P2 code file: [lis4369_p2.R](lis4369_p2.R "P2 Assignment Code Link")
* P2 output file link: [lis4369_p2_requirements_output.txt](lis4369_p2_requirements_output.txt "P2 Assignment Output")
* Screenshots of two plots
* Screenshots of output file 



#### Project Screenshots:

**P2 Output File Screenshots**:



| 1 | 2 | 3 |
|:---:|:---:|:---:|
| ![Output SS 1](img/output1.png) | ![Output SS 2](img/output2.png) | ![Output SS 3](img/output3.png) |


| 4 | 5 | 6 |
|:---:|:---:|:---:|
| ![Output SS 4](img/output4.png) | ![Output SS 5](img/output5.png) | ![Output SS 6](img/output6.png) |



**Plots**:



| Plot 1 | Plot 2 |
|:---:|:---:|
| ![Plot 1](img/plot_disp_and_mpg_1.png) | ![Plot 2](img/plot_disp_and_mpg_2.png) |


**Windows & Data Research**:



| Windows | Data Research |
|:---:|:---:|
| ![Windows ](img/window1.png) | ![Data Research](img/mtcars_data_research.png) |




#### Links:

*This Project - lis4369*:
[P2 for lis4369](https://bitbucket.org/sls16d/lis4369/src/master/p2 "P2 Link")

*lis4369_p2.R file w/ commands*:
[lis4369_p2.R](lis4369_p2.R "Main Assignment")

*lis4369_p2 output file*:
[lis4369_p2_requirements_output.txt](lis4369_p2_requirements_output.txt "P2 R Output File Link") 
