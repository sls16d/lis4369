# Extensive Enterprise Solutions
## LIS 4369

## Sydney Sawyer

### Assignment 5 Requirements:

*Four Parts:*

1. Learn to use R tutorial
2. A5 Assignment
3. Skillsets 13-15
4. Bitbucket repo links:

    a) this project and

    b) Skillsets Folder

#### README.md file should include the following items:

* R tutorial commands file: [learn_to_use.R](r_tutorial/learn_to_use_r.R "A5 R Tutorial Link")
* A5 main assignment link: [lis4369_a5.R](lis4369_a5.R "Main Assignment")
* Screenshots of output from code in RStudio
* Skillset screenshots



#### Assignment Screenshots:

**R tutorial**:



| Graph 1 | Graph 2 | Graph 3 |
|:---:|:---:|:---:|
| ![R Tutorial Graph 1](img/r_tutorial_graph1.png) | ![R Tutorial Graph 2](img/r_tutorial_graph3.png) | ![R Tutorial Graph 3](img/r_tutorial_graph4.png) |



| Windows in RStudio | RStudio Console |
|:---:|:---:|
| ![R Tutorial Window](img/r_tutorial_window.png) | ![R Tutorial Console](img/r_tutorial_console.png) |

#### 
**lis4369_a5.R**:



**Windows & Console**:



| Windows in RStudio | Console Example 1 | Console Example 2 |
|:---:|:---:|:---:|
| ![A5 Window](img/a5_window.png) | ![Console Example 1](img/a5_console1.png) | ![Console Example 2](img/a5_console2.png) |


**Plots**:



| Plot 1 | Plot 2 | Plot 3 |
|:---:|:---:|:---:|
| ![A5 Plot 1](img/a5_plot1.png) | ![A5 Plot 2](img/a5_plot2.png) | ![A5 Plot 3](img/a5_plot3.png) |



**Skillset Screenshots**:



| Skillset 13 | Skillset 14 | Skillset 15 |
|:---:|:---:|:---:|:---:|
| ![Skillset 13](img/ss13_sv_calculator.png) | ![Skillset 14](img/ss14_eh.png) | ![Skillset 15](img/ss15.png) |



#### Links:

*This Project - lis4369*:
[P1 for lis4369](https://bitbucket.org/sls16d/lis4369/src/master/a4 "A4 Link")

*Skillsets Folder*:
[Skillsets Folder](https://bitbucket.org/sls16d/lis4369/src/master/skill_sets/ "Skillsets Link")

*lis4369_a5.R file w/ commands*:
[lis4369_a5.R](lis4369_a5.R "Main Assignment")

*R tutorial file w/ commands*:
[learn_to_use.R](r_tutorial/learn_to_use_r.R "A5 R Tutorial Link") 
