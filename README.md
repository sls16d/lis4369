# Extensive Enterprise Solutions
## LIS 4369

## Sydney Sawyer

### Assignment # Requirements:

*Course Work Links (Relative Link Example):*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Python
    - Install R
    - Install R Studio 
    - Install Visual Studio Code 
    - Create a1_tip_calculator application
    - Create a1 tip calculator in Jupyter Notebook 
    - Provide Screenshots of Installations 
    - Create Bitbucket repo 
    - Complete Bitbucket tutorial 
    - Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Backward-engineer the Payroll Calculator
        - Organize with two modules functions.py and main.py
    - Create payroll calculator in Jupyter Notebook 
    - Backward-engineer three skillsets 
    - Provide screenshots of all work 
3. [A3 README.MD](a3/README.md "My A3 README.md file")
    - Backward-engineer the Painting Estimator 
        - Organize with two modules functions.py and main.py 
        - functions.py must contain the following functions: 
            - get_requirements()
            - estimate_painting_cost()
            - print_painting_estimate()
            - print_painting_percentage()
    - Create Painting Estimator in Jupyter Notebook 
    - Backward-engineer Skillsets (4-6)
        - Calorie Percentage 
        - Python Selections Structures 
        - Python Looping Structures 
    - Provide screenshots for all work
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Install necessary Python packages
    - Backward-engineer Data Analysis 2
    - Create Data Analysis 2 in Jupyter Notebook 
    - Backward-engineer Skillsets (10-12)
        - Python Dictionaries
        - Pseudo-Random Number Generator
        - Temperature Conversion Program
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Complete R Setup & Tutorial 
        - Save as: learn_to_use_r.R
        - Include two plots 
    - Code and Run lis_4369_a5.R
        - Include two plots 
        - Test your program using RStudio
    - Backward-engineer skillsets (13-15)
        - Sphere Volume Calculator
        - Calculator with Error Handling
        - File_Write/Read

### Project # Requirements: 

1. [P1 README.MD](p1/README.md "My P1 README.md file")
    - Update conda
    - Install necessary Python packages 
        - datetime
        - pandas_datareader
        - matplotlib
    - Log in as administrator 
    - Backward-engineer Data Analysis 1
    - Create Data Analysis 1 in Jupyter Notebook
    - Backward-engineer Skillsets (7-9)
        - Python Lists 
        - Python Tuples 
        - Python Sets 
    - Provide screenshots for all work 
2. [P2 README.MD](p2/README.md "My P2 README.md file")
    - Reference Assignment 5 Screenshots 
    - Backward-engineer the lis4369_p2_requirements.txt file
    - Include at least 2 plots 
    - Test your program using RStudio