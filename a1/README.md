# Extensive Enterprise Solutions

## Sydney Sawyer

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations 
3. Questions
4. Bitbucket repo links:

    a) this assignment and

    b) the completed tutorial (bitbuckestationlocations)

#### README.md file should include the following items:

* Screenshot of a1_tip_calculator application running 
* Link to A1 .ipynb file: [tip_calculator.ipynb](a1_tip_calculator/tip_calculator.ipynb "A1 Jupyter Notebook")
* git commands w/ short descriptions 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git merge - Join two or more development histories together

#### Assignment Screenshots:

*Screenshot of "a1_tip_calculator" application running (IDLE)*:

![Tip Calculator Running in IDLE](img/idlescreenshot.jpeg)

*Screenshot of "a1_tip_calculator" application running in (Visual Studio Code)*:

![Tip Calculator Running in VS Code](img/vscodescreenshot.jpeg)

*A1 Jupyter Notebook*:

![A1 in Jupyter Notebook](img/jupyternotebookscreenshot.jpeg)

*Bitbucket Station Location Screenshot*:

![Screenshot Bitbucket Station Location](img/bitbucketstationlocation.PNG)

*A1 Repos Screenshot*:

![Screenshot of a1 Repository](img/lis4369Repo.PNG)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations*:
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/sls16d/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

*This Assignment - lis4369*:
[A1 for lis4369](https://bitbucket.org/sls16d/lis4369/src/master/ "A1 Link")