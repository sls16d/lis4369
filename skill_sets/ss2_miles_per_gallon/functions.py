"""Defines two functions 
1. get_requirements 
2. calculate_mpg
"""


def get_requirements():
    print("Miles Per Gallon")
    print("Developer: Sydney L. Sawyer")

    print("\nProgram Requirements:\n"
        + "1. Convert MPG.\n"
        + "2. Must use float data type for user input and calculation.\n"
        + "3. Format and round conversions to two decimal places.\n")



def calculate_mpg(): 
    miles_driven = 0.0
    gallons_used = 0.0
    MPG = 0.0

    #get user input
    print("Input:")
    miles_driven = float(input("Enter miles driven: "))
    gallons_used = float(input("Enter gallons of fuel used: "))

    if miles_driven <= 0:
        print("Miles cannot be zero!")
    elif gallons_used <= 0:
        print("Gallons cannot be zero!")
    else:
        MPG = round((miles_driven / gallons_used), 2)
        print(miles_driven, "miles driven and", gallons_used, "gallons used =", MPG, "mpg")
        