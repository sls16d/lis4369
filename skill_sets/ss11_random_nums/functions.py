"""Defines two functions
1. get_requirements()
2. random_numbers()
"""
import random as random
def get_requirements():
    print("Pseudo-Random Number Generator")
    print("Developer: Sydney Sawyer")
    print("\nProgram Requirements:\n"
        + "1. Get user beginning and ending integer values, and store in two variables.\n"
        + "2. Display 10 pseudo-random numbers between, and including, above values.\n"
        + "3. Must use integer data types.\n"
        + "4. Example 1: Using range() and randint() functions.\n"
        + "5. Example 2: Using a list with range() and shuffle() functions.\n")

def random_numbers():
    # initialize variables
    start = 0
    end = 0 

    # IPO: Input > Process > Output 
    # get user data
    print("Input: ")
    start = int(input("Enter beginning value: "))
    end = int(input("Enter ending value: "))

    print("\nOutput:")
    print("Example 1: Using range() and randint() functions:")
    for count in range(10):
        print(random.randint(start, end), sep=",", end=" ")
    
    print()

    print("\nExample 2: Using a list with range() and shuffle() functions:")
    r = list(range(start, end + 1))
    random.shuffle(r)
    for i in r:
        print(i, sep=",", end=" ")
    print()

