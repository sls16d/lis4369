"""Defines two functions: 
1. get_requirements()
2. print_selection_structure()
"""
def get_requirements(): 
    print("Python Selections Structures")
    print("Developer: Sydney Sawyer")
    print("\nProgram Requirements:\n"
        + "1. Use Python selections structure.\n"
        + "2. Prompt users for two numbers, and a suitable operator.\n"
        + "3. Test for correct numeric operator.\n"
        + "4. Replicate display below.\n")

def print_selection_structure():
    print("Python Calculator")
    #calculator 
    num1= float(input("Enter num1: "))
    num2= float(input("Enter num2: "))
    print("\nSuitable Operators: +, -, *, /, // (integer division), % (modulo operator), ** (power)")
    op= input("Enter operator: ")

    if op == "+":
        print(num1 + num2)

    elif op == "-":
        print(num1 - num2)

    elif op == "*":
        print(num1 * num2)

    elif op == "/":
        if num2 == 0:
            print("Cannot divide by zero!")
        else: 
            print(num1 / num2)

    elif op == "//": 
        if num2 == 0: 
            print("Cannot divide by zero!")
        else: 
            print(num1 // num2) 

    elif op == "%":
        if num2 == 0:
            print("Cannot divide by zero!")
        else: 
            print(num1 % num2)

    elif op == "**":
        print(num1 ** num2)

    else: 
        print("Incorrect operator!") 