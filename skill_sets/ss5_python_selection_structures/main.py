import functions as f

def main():
    f.get_requirements()
    f.print_selection_structure()

if __name__ == "__main__":
    main()