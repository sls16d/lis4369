# Extensive Enterprise Solutions

## Sydney Sawyer

### Assignment 3 Requirements:

*Three Parts:*

1. Backward-engineer (Python)
2. Organize programs with two modules 
3. Bitbucket repo links:

    a) this assignment and

    b) Skillsets Folder

#### README.md file should include the following items:

* Screenshot of a3_painting_estimator application running (IDLE, VS Code)
* Link to A3 .ipynb file: [a3_painting_estimator.ipynb](a3_painting_estimator/a3_painting_estimator.ipynb "A3 Jupyter Notebook")
* Screenshots of Skillsets (4-6) 


#### Assignment Screenshots:

|*Painting Estimator in VSCode*: |*Painting Estimator in Idle*: 	|
|:-:	|:-:	|
| ![Painting Estimator Running in VSCods](img/a3_painting_calculator_screenshot.png) 	|![Painting Estimator Running in IDLE](img/a3_painting_estimator_idlesc.png) 	|


*A3 Jupyter Notebook*:

|Screenshot 1 |Screenshot 2 	|
|:-:	|:-:	|
|![A3 in Jupyter Notebook 1](img/a3_jupyternotebook_screenshot1.png) 	|![A3 in Jupyter Notebook 2](img/a3_jupyternotebook_screenshot2.png) 	|

*Skillset Screenshots*:

Skillset 4

![Skillset 4](img/ss4_screenshot.png) 

Skillset 5

![Skillset 5](img/ss5_screenshot.png) 

Skillset 6
 
![Skillset 6](img/ss6_screenshot.png) 


#### Links:

*This Assignment - lis4369*:
[A3 for lis4369](https://bitbucket.org/sls16d/lis4369/src/master/a3 "A3 Link")

*Skillsets Folder*:
[Skillsets Folder](https://bitbucket.org/sls16d/lis4369/src/master/skill_sets/ "Skillsets Link")

*A3 Jupyter Notebook .ipybn*:
[a3_payroll.ipynb](a3_painting_estimator/a3_painting_estimator.ipynb "A3 Jupyter Notebook")
