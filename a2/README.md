# Extensive Enterprise Solutions

## Sydney Sawyer

### Assignment 2 Requirements:

*Three Parts:*

1. Backward-engineer (Python)
2. Organize programs with two modules 
3. Bitbucket repo links:

    a) this assignment and

    b) Skillsets Folder

#### README.md file should include the following items:

* Screenshot of a2_payroll_calculator application running (IDLE, VS Code)
* Link to A2 .ipynb file: [a2_payroll.ipynb](a2_payroll_calculator/a2_payroll.ipynb "A2 Jupyter Notebook")
* Screenshots of Skillsets (1-3) 


#### Assignment Screenshots:

|*Payroll Calculator with Overtime*: |*Payroll Calculator No Overtime*: 	|
|:-:	|:-:	|
| ![Payroll Calculator Running in IDLE](img/payroll_idle.png) 	|![Payroll Calculator Running in VS Code](img/no_overtime.png) 	|


*A2 Jupyter Notebook*:

![A2 in Jupyter Notebook](img/a2_jupyter_payroll.png)

*Skillset 1*:

![Skillset 1](img/ss1_sq_ft_to_acres.png)

*Skillset 2*:

![Skillset 2](img/ss2_miles_per_gallon.png)

*Skillset 3*:

![Skillset 3](img/ss3_it_ict_students.png)


#### Links:

*This Assignment - lis4369*:
[A2 for lis4369](https://bitbucket.org/sls16d/lis4369/src/master/a2 "A2 Link")

*Skillsets Folder*:
[Skillsets Folder](https://bitbucket.org/sls16d/lis4369/src/master/skill_sets/ "Skillsets Link")

*A2 Jupyter Notebook .ipybn*:
[a2_payroll.ipynb](a2_payroll_calculator/a2_payroll.ipynb "A2 Jupyter Notebook")