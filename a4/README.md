# Extensive Enterprise Solutions
## LIS 4369

## Sydney Sawyer

### Assignment 4 Requirements:

*Three Parts:*

1. Backward-engineer Data Analysis 2 (Python)
2. Skillsets (10-12)
3. Bitbucket repo links:

    a) this project and

    b) Skillsets Folder

#### README.md file should include the following items:

* Screenshot of a4_data_analysis_2 application running
* Link to A4 .ipynb file: [a4_data_analysis_2.ipynb](a4_data_analysis_2/a4_jupyter_notebook.ipynb "A4 Jupyter Notebook")
* Screenshots of Skillsets (10-12) 


#### Project Screenshots:

**Data Analysis 2**:



| 1 | 2 | 3 |
|:---:|:---:|:---:|
| ![A4 Code 1](img/code1.png) | ![A4 Code 2](img/code2.png) | ![A4 Code 3](img/code3.png) |



| 4 | 5 | Graph |
|:---:|:---:|:---:|
| ![A4 Code 4](img/code4.png) | ![A4 Code 5](img/code5.png) | ![A4 Graph](img/graph.png) |

#### 
**P1 Jupyter Notebook**:



**Code**:



| 1 | 2 | 3 | 4 |
|:---:|:---:|:---:|:---:|
| ![Code Screenshot 1](img/jup_notebook1.png) | ![Code Screenshot 2](img/jup_notebook2.png) | ![Code Screenshot 3](img/jup_notebook3.png) | ![Code Screenshot 4](img/jup_notebook4.png) |


**Results**:



| 1 | 2 | 3 | 4 |
|:---:|:---:|:---:|:---:|
| ![Results Screenshot 1](img/jup_results1.png) | ![Results Screenshot 2](img/jup_results2.png) | ![Results Screenshot 3](img/jup_results3.png) | ![Results Screenshot 4](img/jup_results4.png) |



**Skillset Screenshots**:



| Skillset 10 | Skillset 11 | Skillset 12 |
|:---:|:---:|:---:|:---:|
| ![Skillset 10](img/ss10.png) | ![Skillset 11](img/ss11.png) | ![Skillset 10](img/ss12.png) |



#### Links:

*This Project - lis4369*:
[P1 for lis4369](https://bitbucket.org/sls16d/lis4369/src/master/a4 "A4 Link")

*Skillsets Folder*:
[Skillsets Folder](https://bitbucket.org/sls16d/lis4369/src/master/skill_sets/ "Skillsets Link")

*A4 Jupyter Notebook .ipybn*:
[a4_data_analysis_2.ipynb](a4_data_analysis_2/a4_jupyter_notebook.ipynb "A4 Jupyter Notebook") 
